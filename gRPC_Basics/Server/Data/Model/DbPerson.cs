﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Data.Model
{
    public class DbPerson
    {
        public int Id { get; set; }
        public string Fname { get; set; }
        public string Lname { get; set; }
    }
}
